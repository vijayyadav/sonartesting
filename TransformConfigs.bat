@ECHO OFF

:: Resources
set cttHelper="C:\buildtools\Transform\Equinox.CTTHelper.exe"
set ctt="c:\BuildTools\Transform\ctt.exe" 

:: Set visual studio project file
set projectFile=".\src\Hypergate.Equifit\Hypergate.Equifit.csproj"

:: Check input arguments if environment is passed in
If "%1" == "" GOTO Prompt

ECHO (%1)

SET userInput=%1
:ValidateParameter
If /I %userInput%==development GOTO SetValidEnvironment
If /I %userInput%==qa GOTO SetValidEnvironment
If /I %userInput%==staging GOTO SetValidEnvironment
If /I %userInput%==preproduction GOTO SetValidEnvironment
If /I %userInput%==production GOTO SetValidEnvironment


ECHO.
ECHO Invalid environment parameter
ECHO Usage: TransformConfig.bat [environment]
ECHO Parameters:
ECHO       [environment] : developement
ECHO                       qa
ECHO                       staging
ECHO                       preproduction
ECHO                       production
ECHO.

:: No valid environment set, prompt for input
GOTO EOF



:ExecuteTransform
ECHO ----------------------------------------------------------
ECHO Tranforming configs to %env% environment
ECHO ----------------------------------------------------------
ECHO.
%cttHelper% ctt:%ctt% p:%projectFile% c:"%env%"
ECHO %cttHelper% ctt:%ctt% p:%projectFile% c:"%env%"
GOTO EOF


:Prompt
ECHO This will transform all config files in the envioronment to the target environment.
ECHO.
ECHO Select a target environment:
ECHO 1) Development
ECHO 2) QA
ECHO 3) Staging
ECHO 4) Pre-production
ECHO 5) Production
ECHO Q) Quit
ECHO.
:PromptInput
SET /P userInput="Select environment: "

IF /I "%userInput%"=="q" GOTO EOF
IF "%userinput%"=="1" GOTO SetValidEnvironment
IF "%userinput%"=="2" GOTO SetValidEnvironment
IF "%userinput%"=="3" GOTO SetValidEnvironment
IF "%userinput%"=="4" GOTO SetValidEnvironment
IF "%userinput%"=="5" GOTO SetValidEnvironment

:: Invalid input, prompt again
ECHO Invalid input.
ECHO.
GOTO PromptInput

:SetValidEnvironment
::Environment configuration names are case sensitive
IF "%userinput%"=="1" SET env=Development
IF /I "%userinput%"=="development" SET env=Development
IF "%userinput%"=="2" SET env=QA
IF /I "%userinput%"=="qa" SET env=QA
IF "%userinput%"=="3" SET env=Staging
IF /I "%userinput%"=="staging" SET env=Staging
IF "%userinput%"=="4" SET env=PreProduction
IF /I "%userinput%"=="preproduction" SET env=PreProduction
IF "%userinput%"=="5" SET env=Production
IF /I "%userinput%"=="production" SET env=Production

GOTO ExecuteTransform

:EOF
IF NOT "%1" == "" GOTO :EOF_FINAL
pause

:EOF_FINAL