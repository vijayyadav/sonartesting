﻿using System.Net.Http;
using System.Web.Http;
using System.Collections.Generic;
using System.Linq;
using Hypergate.Equifit.Areas.V1.Controllers;
using Hypergate.Equifit.Repository.Database;
using Hypergate.Equifit.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using Moq;

namespace Hypergate.Equifit.Tests.Unit
{
    [TestClass]
    public class EquifitUnitTests
    {
        // Arrange
        string memberId = "1000066296";
        private int country = 1;
        private Mock<IEquifitRepository> moq_EquifitRepository;
        private EquifitController equifitController;

        [TestInitialize]
        public void Initialize()
        {
            moq_EquifitRepository =
                new Mock<IEquifitRepository>();
            equifitController = new EquifitController(moq_EquifitRepository.Object)
            {
                Request = new HttpRequestMessage(),
                Configuration = new HttpConfiguration()
            };
        }

        [TestMethod]
        public void Get_PreEquifitEligibility_ShouldReturn_EligibilityForUser()
        {
            var response = false;

            moq_EquifitRepository.Setup(p => p.GetPreEquifitEligibility(memberId, country)).Returns(response);
            var output = equifitController.CheckPreEquifitEligibility(memberId, country);
            Assert.IsTrue(output.IsSuccessStatusCode);
            var content = output.Content.ReadAsStringAsync();
            content.Wait();
            var result = JsonConvert.DeserializeObject<bool>(content.Result);
            Assert.AreEqual(result, response);
        }

        [TestMethod]
        public void Get_PreEquifitQuestionnaire_ShouldReturn_Questionnaire()
        {
            var response = new List<EquifitDocumentTemplate>();
            moq_EquifitRepository.Setup(p => p.GetPreEquifitQuestionnaire()).Returns(response);
            var output = equifitController.GetPreEquifitQuestionnaire();
            Assert.IsTrue(output.IsSuccessStatusCode);
            var content = output.Content.ReadAsStringAsync();
            content.Wait();
            var result = JsonConvert.DeserializeObject<PreEquifitQuestionnaireResponse>(content.Result);
            Assert.AreEqual(result, response);
        }
    }
}
