﻿<?xml version="1.0"?>
<!--
  For more information on how to configure your ASP.NET application, please visit
  https://go.microsoft.com/fwlink/?LinkId=301879
  -->
<configuration>
  <configSections>
    <!-- For more information on Entity Framework configuration, visit http://go.microsoft.com/fwlink/?LinkID=237468 -->
    <section name="entityFramework" type="System.Data.Entity.Internal.ConfigFile.EntityFrameworkSection, EntityFramework, Version=6.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" requirePermission="false"/>
    <section name="log4net" type="log4net.Config.Log4NetConfigurationSectionHandler, log4net"/>
    <section name="nlog" type="NLog.Config.ConfigSectionHandler, NLog"/>
  </configSections>
  <appSettings configSource="Configuration\appSettings.config"/>
  <connectionStrings configSource="Configuration\ConnectionStrings.config"/>
  <log4net configSource="Configuration\log4net.config"/>
  <!--
    For a description of web.config changes see http://go.microsoft.com/fwlink/?LinkId=235367.

    The following attributes can be set on the <httpRuntime> tag.
      <system.Web>
        <httpRuntime targetFramework="4.5.2" />
      </system.Web>
  -->
  <system.web>
    <compilation debug="true" targetFramework="4.5.2"/>
    <httpRuntime targetFramework="4.5.2"/>
    <trace enabled="false" pageOutput="false" requestLimit="40" localOnly="false"/>
  </system.web>
  <system.webServer>
    <rewrite>
      <rules>
        <rule name="RewriteStaticSwaggerFile">
          <match url="v1/swagger.json"/>
          <action type="Rewrite" url="Areas/V1/Public/swagger.json"/>
        </rule>
      </rules>
    </rewrite>
    <handlers>
      <remove name="ExtensionlessUrlHandler-Integrated-4.0"/>
      <remove name="OPTIONSVerbHandler"/>
      <remove name="TRACEVerbHandler"/>
      <add name="ExtensionlessUrlHandler-Integrated-4.0" path="*." verb="*" type="System.Web.Handlers.TransferRequestHandler" preCondition="integratedMode,runtimeVersionv4.0"/>
    </handlers>
  </system.webServer>
  <runtime>
    <assemblyBinding xmlns="urn:schemas-microsoft-com:asm.v1">
      <dependentAssembly>
        <assemblyIdentity name="Newtonsoft.Json" publicKeyToken="30ad4fe6b2a6aeed" culture="neutral"/>
        <bindingRedirect oldVersion="0.0.0.0-9.0.0.0" newVersion="9.0.0.0"/>
      </dependentAssembly>
      <dependentAssembly>
        <assemblyIdentity name="System.Web.Http" publicKeyToken="31bf3856ad364e35" culture="neutral"/>
        <bindingRedirect oldVersion="0.0.0.0-5.2.3.0" newVersion="5.2.3.0"/>
      </dependentAssembly>
      <dependentAssembly>
        <assemblyIdentity name="System.Net.Http.Formatting" publicKeyToken="31bf3856ad364e35" culture="neutral"/>
        <bindingRedirect oldVersion="0.0.0.0-5.2.3.0" newVersion="5.2.3.0"/>
      </dependentAssembly>
      <dependentAssembly>
        <assemblyIdentity name="System.Web.Helpers" publicKeyToken="31bf3856ad364e35"/>
        <bindingRedirect oldVersion="1.0.0.0-3.0.0.0" newVersion="3.0.0.0"/>
      </dependentAssembly>
      <dependentAssembly>
        <assemblyIdentity name="System.Web.WebPages" publicKeyToken="31bf3856ad364e35"/>
        <bindingRedirect oldVersion="1.0.0.0-3.0.0.0" newVersion="3.0.0.0"/>
      </dependentAssembly>
      <dependentAssembly>
        <assemblyIdentity name="System.Web.Mvc" publicKeyToken="31bf3856ad364e35"/>
        <bindingRedirect oldVersion="1.0.0.0-5.2.3.0" newVersion="5.2.3.0"/>
      </dependentAssembly>
      <dependentAssembly>
        <assemblyIdentity name="WebGrease" publicKeyToken="31bf3856ad364e35" culture="neutral"/>
        <bindingRedirect oldVersion="0.0.0.0-1.5.2.14234" newVersion="1.5.2.14234"/>
      </dependentAssembly>
      <dependentAssembly>
        <assemblyIdentity name="log4net" publicKeyToken="669e0ddf0bb1aa2a" culture="neutral"/>
        <bindingRedirect oldVersion="0.0.0.0-2.0.7.0" newVersion="2.0.7.0"/>
      </dependentAssembly>
    </assemblyBinding>
  </runtime>
  <!--<system.serviceModel>
    <serviceHostingEnvironment aspNetCompatibilityEnabled="true" multipleSiteBindingsEnabled="true" />
    -->
  <!--<behaviors configSource="Configuration\system.serviceModel.behaviors.config" />-->
  <!--
    <bindings configSource="Configuration\system.serviceModel.bindings.config" />
    <client configSource="Configuration\system.serviceModel.client.config" />
    -->
  <!--<extensions configSource="Configuration\system.serviceModel.extensions.config" />-->
  <!--
  </system.serviceModel>-->
  <entityFramework>
    <defaultConnectionFactory type="System.Data.Entity.Infrastructure.LocalDbConnectionFactory, EntityFramework">
      <parameters>
        <parameter value="mssqllocaldb"/>
      </parameters>
    </defaultConnectionFactory>
    <providers>
      <provider invariantName="System.Data.SqlClient" type="System.Data.Entity.SqlServer.SqlProviderServices, EntityFramework.SqlServer"/>
    </providers>
  </entityFramework>
  <nlog xsi:schemaLocation="http://www.nlog-project.org/schemas/NLog.xsd NLog.xsd" autoReload="true" throwExceptions="true" internalLogFile="c:\logs\nlog-internal.txt" xmlns="http://www.nlog-project.org/schemas/NLog.xsd" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
    <extensions>
      <add assembly="NLog.Targets.ElasticSearch"/>
    </extensions>
    <targets>
      <target name="n" xsi:type="AsyncWrapper">
        <target xsi:type="RetryingWrapper">
          <target name="File" xsi:type="File" fileName="C:\logs\dev-hypergate-equifit\log-${shortdate}.txt" layout="${stacktrace} ${message}"/>
        </target>
      </target>
      <target xsi:type="AsyncWrapper" name="AsyncElastic" queueLimit="5000" overflowAction="Discard">
        <target xsi:type="ElasticSearch" layout="${logger} | ${threadid} | ${message}" includeAllProperties="true" name="Elastic" uri="http://search-digital-apps-d6qooxlyatgv6mji3k55spxgay.us-east-1.es.amazonaws.com" index="dev-hypergate-logs">
          <field name="user" layout="${windows-identity:userName=True:domain=False}"/>
          <field name="host" layout="${machinename}"/>
          <field name="hyper-microsite" layout="${iis-site-name}"/>
          <field name="aspnet-requestmethod" layout="${aspnet-Request-Method} "/>
          <field name="aspnet-requestreferrer" layout="${aspnet-Request-Referrer} "/>
          <field name="aspnet-requesturl" layout="${aspnet-Request-Url} "/>
          <field name="threadname" layout="${threadname}"/>
        </target>
      </target>
    </targets>
    <rules>
      <logger name="*" minlevel="Off" writeTo="AsyncElastic"/>
    </rules>
  </nlog>
</configuration>