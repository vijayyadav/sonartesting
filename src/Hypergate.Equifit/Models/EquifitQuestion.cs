﻿using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using System.Xml.XPath;
using Hypergate.Equifit.Helper.Extensions;

namespace Hypergate.Equifit.Models
{
    public class EquifitQuestion
    {
        public EquifitQuestion(XElement element)
        {

            this.Id = element.GetAttributeValueOrDefault<string>(EquifitAttribute.Id.ToName());
            this.Text = element.Element(EquifitElement.Text.ToName()).GetValueOrDefault<string>();
            this.ParentId = element.GetAttributeValueOrDefault<string>(EquifitAttribute.Parent.ToName());
            this.CssClass = element.GetAttributeValueOrDefault<string>(EquifitAttribute.CssClass.ToName());
            this.DisplayOrder = element.GetAttributeValueOrDefault<double?>(EquifitAttribute.DisplayOrder.ToName());
            this.IsPreEquifit = element.GetAttributeValueOrDefault<bool>(EquifitAttribute.IsPreEquifit.ToName());
            this.IsClientReport = element.GetAttributeValueOrDefault<bool>(EquifitAttribute.IsClientReport.ToName());
            this.AnswerType = new EquifitAnswerType(element.Element(EquifitElement.AnswerType.ToName()));            
            this.Content = GetContent(element);
        }

        public string Id { get; private set; }
        public string Text { get; private set; }
        public string ParentId { get; private set; }
        public string CssClass { get; private set; }
        public double? DisplayOrder { get; private set; }
        public bool IsPreEquifit { get; private set; }
        public bool IsClientReport { get; private set; }
        public EquifitAnswerType AnswerType { get; private set; }        
        public IDictionary<string, string> Content { get; private set; }       

        private string GetAnswerDisplayText(XElement questionTemplate, string answerId)
        {
            string displayText = null;
            var xpath = string.Format("//Question/AnswerType/AnswerOptions/AnswerOption[@Id='{0}']",
                answerId.Replace("'", ""));

            var element = questionTemplate.XPathSelectElement(xpath);
            if (element != null && element.HasAttributes)
            {
                var attribute = element.Attributes().SingleOrDefault(x => x.Name == "DisplayText");
                if (attribute != null)
                {
                    displayText = attribute.Value;
                }
            }
            return displayText ?? "";
        }

        private IEnumerable<string> GetAnswersForQuestion(string answerId, IQueryable<XElement> answers)
        {
            return
                answers.Where(element => element.GetAttributeValueOrDefault<string>("Id") == answerId)
                    .AsQueryable()
                    .Select(x => x.Value)
                    .ToList();
        }

        private static IDictionary<string, string> GetContent(XElement paragraph)
        {
            return paragraph.Descendants(EquifitElement.Paragraph.ToName()).ToDictionary(key =>
                key.Element(EquifitElement.Title.ToName()).GetValueOrDefault<string>(), value =>
                    value.Element(EquifitElement.Text.ToName()).GetValueOrDefault<string>());
        }
    }
}