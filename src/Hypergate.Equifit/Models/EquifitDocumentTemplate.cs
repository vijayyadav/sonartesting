﻿using System.Xml.Linq;

namespace Hypergate.Equifit.Models
{
    public class EquifitDocumentTemplate
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public decimal Version { get; set; }
        public string Template { get; set; }
        public XElement TemplateXml
        {
            get { return string.IsNullOrEmpty(this.Template) ? null : XElement.Parse(this.Template); }
            set { this.Template = value.ToString(); }
        }
    }
}