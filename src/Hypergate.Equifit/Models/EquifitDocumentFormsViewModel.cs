﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Hypergate.Equifit.Models
{
    public class EquifitDocumentFormsViewModel
    {
        public EquifitDocumentFormsViewModel(JObject schema, JArray fieldsets, int templateId, string title, decimal version)            
        {
            this.Schema = schema;            
            this.Fieldsets = fieldsets;            
            this.TemplateId = templateId;
            this.Title = title;
            this.Version = version;
        }
        
        public JObject Schema { get; private set; }        
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]        
        public JArray Fieldsets { get; private set; }
        public int? TemplateId { get; set; }
        public string Title { get; set; }
        public int QuestionsCount { get; set; }
        public decimal Version { get; set; }
    }
}