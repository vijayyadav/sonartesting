﻿using Hypergate.Equifit.Helper.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Xml.Linq;

namespace Hypergate.Equifit.Models
{
    public class EquifitForm
    {        
        private readonly IList<Expression<Func<XElement, bool>>> _filters = new List<Expression<Func<XElement, bool>>>();
        private readonly EquifitDocumentTemplate _template;
        private IQueryable<XElement> _queryableAnswers;
        private IQueryable<XElement> _queryableQuestions;
        private IQueryable<XElement> _queryableSections;

        public EquifitForm(EquifitDocumentTemplate template)
        {
            this._template = template;           
            this.Title = this._template.Title;            
        }
        
        public string Title { get; private set; }
       
        public IEnumerable<EquifitSection> Sections { get; private set; }

        public virtual EquifitForm Render()
        {
            this.Sections =
                this.QueryableSections.Select(
                    element => new EquifitSection(element, this._filters)).ToList();

            return this;
        }

        public virtual EquifitForm Filter(Expression<Func<XElement, bool>> predicate)
        {
            this.Include(predicate);

            return this;
        }

        #region Helpers

        protected virtual IQueryable<XElement> QueryableSections
        {
            get
            {
                return this._queryableSections ??
                       (this._queryableSections =
                           this._template.TemplateXml.Descendants(EquifitElement.Section.ToName())
                               .AsQueryable());
            }
        }

        protected virtual IQueryable<XElement> QueryableQuestions
        {
            get
            {
                return this._queryableQuestions ??
                       (this._queryableQuestions =
                           this._template.TemplateXml.Descendants(EquifitElement.Question.ToName())
                               .AsQueryable());
            }
        }
        
        private void Include(Expression<Func<XElement, bool>> predicate)
        {
            this._filters.Add(predicate);
        }

        #endregion
    }
}