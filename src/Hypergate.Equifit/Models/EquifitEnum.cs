﻿namespace Hypergate.Equifit.Models
{
    public enum EquifitElement
    {
        Document,
        Section,
        Description,
        Question,
        Content,
        Paragraph,
        Answers,
        Answer,
        AnswerType,
        AnswerOptions,
        AnswerOption,
        Events,
        Event,
        Targets,
        Target,
        Conditions,
        Condition,
        Validators,
        Validator,
        Title,
        Text
    }

    public enum EquifitAttribute
    {
        Id,
        Name,
        Text,
        Version,
        DisplayOrder,
        CssClass,
        Default,
        Placeholder,
        ReadOnly,
        Disabled,
        IsPreEquifit,
        IsClientReport,
        Parent,
        InputType,
        HelpText,
        DisplayText,
        Action,
        Type,
        ErrorMessage,
        StartYearOffset,
        EndYearOffset,
        MaxLength,
        StartYear
    }

    public enum EquifitConditionType
    {
        IfTrue
    }

    public enum EquifitValidatorType
    {
        Required
    }
}