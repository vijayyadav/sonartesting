﻿using Hypergate.Equifit.Helper.Extensions;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;

namespace Hypergate.Equifit.Models
{
    public class EquifitAnswerEvent
    {
        public EquifitAnswerEvent(XElement element)
        {

            this.Action = element.GetAttributeValueOrDefault<string>(EquifitAttribute.Action.ToName());
            this.Targets = this.ToAnswerTargets(element).ToList();
            this.Conditions = this.ToAnswerConditions(element).ToList();
        }

        public string Action { get; private set; }
        public IEnumerable<EquifitAnswerTarget> Targets { get; private set; }
        public IEnumerable<EquifitAnswerCondition> Conditions { get; private set; }
    }
}