﻿using Hypergate.Equifit.Helper.Extensions;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;

namespace Hypergate.Equifit.Models
{
    public class EquifitAnswerType
    {
        public EquifitAnswerType(XElement element)
        {
            this.CssClass = element.GetAttributeValueOrDefault<string>(EquifitAttribute.CssClass.ToName());
            this.Default = element.GetAttributeValueOrDefault<string>(EquifitAttribute.Default.ToName());
            this.Placeholder = element.GetAttributeValueOrDefault<string>(EquifitAttribute.Placeholder.ToName());
            this.ReadOnly = element.GetAttributeValueOrDefault<string>(EquifitAttribute.ReadOnly.ToName());
            this.Disabled = element.GetAttributeValueOrDefault<string>(EquifitAttribute.Disabled.ToName());
            this.InputType = element.GetAttributeValueOrDefault<string>(EquifitAttribute.InputType.ToName());
            this.HelpText = element.GetAttributeValueOrDefault<string>(EquifitAttribute.HelpText.ToName());
            this.MaxLength = element.GetAttributeValueOrDefault<int?>(EquifitAttribute.MaxLength.ToName());
            this.StartYearOffset = element.GetAttributeValueOrDefault<int?>(EquifitAttribute.StartYearOffset.ToName());
            this.EndYearOffset = element.GetAttributeValueOrDefault<int?>(EquifitAttribute.EndYearOffset.ToName());
            this.StartYear = element.GetAttributeValueOrDefault<int?>(EquifitAttribute.StartYear.ToName());
            this.AnswerOptions = this.ToAnswerOptions(element).ToList();
            this.Events = this.ToAnswerEvents(element).ToList();
            this.Validators = this.ToValidators(element).ToList();
        }

        public string CssClass { get; private set; }
        public string Default { get; private set; }
        public string Placeholder { get; private set; }
        public string ReadOnly { get; private set; }
        public string Disabled { get; private set; }
        public string InputType { get; private set; }
        public string HelpText { get; private set; }
        public int? MaxLength { get; private set; }
        public int? StartYearOffset { get; private set; }
        public int? EndYearOffset { get; private set; }
        public int? StartYear { get; private set; }
        public IEnumerable<EquifitAnswerOption> AnswerOptions { get; private set; }
        public IEnumerable<EquifitAnswerEvent> Events { get; private set; }
        public IEnumerable<EquifitValidator> Validators { get; private set; }
    }

}