﻿namespace Hypergate.Equifit.Models
{
    public class EquifitAnswerCondition
    {
        public EquifitAnswerCondition(EquifitConditionType conditionType, string target)
        {

            this.ConditionType = conditionType;
            this.Target = target;
        }

        public EquifitConditionType ConditionType { get; private set; }
        public string Target { get; private set; }
    }
}