﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Xml.Linq;
using Hypergate.Equifit.Helper.Extensions;

namespace Hypergate.Equifit.Models
{
    public class EquifitSection
    {
        public EquifitSection(XElement element,
           IEnumerable<Expression<Func<XElement, bool>>> filters)
        {
            this.Name = element.GetAttributeValueOrDefault<string>(EquifitAttribute.Name.ToName()) ??
                   element.Element(EquifitElement.Description.ToName()).GetValueOrDefault<string>();
            this.Questions =
                ApplyFiltersToQuestions(element, filters)
                    .Select(question => new EquifitQuestion(question))
                    .ToList();
        }

        public string Name { get; private set; }
        public IEnumerable<EquifitQuestion> Questions { get; private set; }

        private static IQueryable<XElement> ApplyFiltersToQuestions(XElement element,
            IEnumerable<Expression<Func<XElement, bool>>> filters)
        {
            var questions = element.Descendants(EquifitElement.Question.ToName()).AsQueryable();

            filters.Each(predicate => { questions = questions.Where(predicate); });
            
            var questionIds = new List<string>();
            var preEquifitQuestions = questions.Where(question => question.GetAttributeValueOrDefault<bool?>(EquifitAttribute.IsPreEquifit.ToName()) == true);
            foreach (var q in preEquifitQuestions)
            {
                var id = q.GetAttributeValueOrDefault<string>(EquifitAttribute.Id.ToName());
                questionIds.Add(id);
                var childQuestions = questions.Where(question => question.GetAttributeValueOrDefault<string>(EquifitAttribute.Parent.ToName())==id);
                childQuestions.ToList().ForEach(c =>
                {
                    questionIds.Add(c.GetAttributeValueOrDefault<string>(EquifitAttribute.Id.ToName()));
                });
            }
            questions = questions.Where(question => questionIds.Contains(question.GetAttributeValueOrDefault<string>(EquifitAttribute.Id.ToName())));
            
            if (
                questions.Any(
                    question =>
                        question.GetAttributeValueOrDefault<double?>(EquifitAttribute.DisplayOrder.ToName()) != null))
            {
                questions =
                    questions.OrderBy(
                        question => question.GetAttributeValueOrDefault<double?>(EquifitAttribute.DisplayOrder.ToName()));
            }

            return questions;
        }
    }
}