﻿namespace Hypergate.Equifit.Models
{
    public class EquifitAnswerTarget
    {
        public EquifitAnswerTarget(string target)
        {
            this.Target = target;
        }

        public string Target { get; private set; }
    }
}