﻿namespace Hypergate.Equifit.Models
{
    public class EquifitValidator
    {
        public EquifitValidator(EquifitValidatorType type, string errorMessage)
        {

            this.Type = type;
            this.ErrorMessage = errorMessage;
        }
        public EquifitValidatorType Type { get; private set; }
        public string ErrorMessage { get; private set; }
    }
}