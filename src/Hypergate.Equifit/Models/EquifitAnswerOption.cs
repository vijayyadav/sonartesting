﻿namespace Hypergate.Equifit.Models
{
    public class EquifitAnswerOption
    {
        public EquifitAnswerOption(string id, string displayText)
        {
            this.Id = id;
            this.DisplayText = displayText;
        }

        public string Id { get; private set; }
        public string DisplayText { get; private set; }
    }
}