﻿using System.Collections.Generic;

namespace Hypergate.Equifit.Models
{
    public class PreEquifitQuestionnaireResponse
    {
        public PreEquifitQuestionnaireResponse()
        {
            Schemas = new List<EquifitDocumentFormsViewModel>();
        }
        public List<EquifitDocumentFormsViewModel> Schemas { get; set; }
        public int TotalQuestions { get; set; }
    }
}