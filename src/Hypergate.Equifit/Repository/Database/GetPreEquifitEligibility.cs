﻿using System;
using System.Data;
using System.Data.SqlClient;

namespace Hypergate.Equifit.Repository.Database
{
    public partial class EquifitRepository
    {
        public bool GetPreEquifitEligibility(string memberId, int country)
        {
            var result = false;
            using (var connection = new SqlConnection(MrDatabaseConnectionString))
            {
                if (connection.State == System.Data.ConnectionState.Closed)
                    connection.Open();
                var command = new SqlCommand()
                {
                    CommandType = System.Data.CommandType.StoredProcedure,
                    Connection = connection,
                    CommandText = "dbo.sp_Equifit_GetUserEligibility"
                };
                command.Parameters.Add(new SqlParameter("@memberId", memberId));
                command.Parameters.Add(new SqlParameter("@country", country));

                var reader = command.ExecuteReader();
                var dt = new DataTable();
                dt.Load(reader);
                if (dt.Rows.Count > 0)
                    result = Convert.ToBoolean(dt.Rows[0]["IsPreEquifitEligible"]);
            }

            return result;
        }
    }
}