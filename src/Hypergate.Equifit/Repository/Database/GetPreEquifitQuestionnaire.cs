﻿using System;
using System.Data.SqlClient;
using System.Collections.Generic;
using Hypergate.Equifit.Models;

namespace Hypergate.Equifit.Repository.Database
{
    public partial class EquifitRepository
    {
        public List<EquifitDocumentTemplate> GetPreEquifitQuestionnaire()
        {
            var result = new List<EquifitDocumentTemplate>();
            using (var connection = new SqlConnection(WebDatabaseConnectionString))
            {
                if (connection.State == System.Data.ConnectionState.Closed)
                    connection.Open();
                var command = new SqlCommand()
                {
                    CommandType = System.Data.CommandType.StoredProcedure,
                    Connection = connection,
                    CommandText = "dbo.sp_Equifit_GetPreEquifitQuestionnaire"
                };
                
                var reader = command.ExecuteReader();               
                while (reader.Read())
                {
                    var docTemplate = new EquifitDocumentTemplate();
                    docTemplate.Id =  Convert.ToInt32(reader["Id"]);
                    docTemplate.Title = reader["Title"].ToString();
                    docTemplate.Version = Convert.ToDecimal(reader["Version"]);
                    docTemplate.Template = reader["Template"].ToString();
                    result.Add(docTemplate);
                }                    
            }

            return result;
        }
    }
}