﻿using System.Configuration;

namespace Hypergate.Equifit.Repository.Database
{
    public partial class EquifitRepository : IEquifitRepository
    {
        protected string MrDatabaseConnectionString;
        protected string WebDatabaseConnectionString;
		public EquifitRepository()
        {
            const string mrDatabaseConnectionString = "MrDatabaseEntities";
            const string webDatabaseConnectionString = "WebDatabaseEntities";

			var mrdatabaseConnectionStringSettings = ConfigurationManager.ConnectionStrings[mrDatabaseConnectionString];            
            var webdatabaseConnectionStringSettings = ConfigurationManager.ConnectionStrings[webDatabaseConnectionString];

            MrDatabaseConnectionString = mrdatabaseConnectionStringSettings.ConnectionString;
            WebDatabaseConnectionString = webdatabaseConnectionStringSettings.ConnectionString;
		}
    }
}