﻿using System.Collections.Generic;
using Hypergate.Equifit.Models;

namespace Hypergate.Equifit.Repository.Database
{
	public interface IEquifitRepository
    {
		bool GetPreEquifitEligibility(string memberId, int country);
        List<EquifitDocumentTemplate> GetPreEquifitQuestionnaire();
	}
}