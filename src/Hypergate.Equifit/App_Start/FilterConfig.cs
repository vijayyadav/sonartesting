﻿using System.Web;
using System.Web.Mvc;
using Hypergate.Equifit.Filters;

namespace Hypergate.Equifit
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new TokenAuthAttribute());
        }
    }
}
