﻿using System.Configuration;
using System.Linq;

namespace Hypergate.Equifit.Filters
{
    public class TokenAuthAttribute : System.Web.Http.Filters.ActionFilterAttribute
    {
        public override void OnActionExecuting(System.Web.Http.Controllers.HttpActionContext actionContext)
        {
            if (!actionContext.Request.Headers.Any(x => x.Key == ConfigurationManager.AppSettings["AuthKeyName"]))
            {
                actionContext.Response = new System.Net.Http.HttpResponseMessage(System.Net.HttpStatusCode.Unauthorized);
            }
            else
            {
                var headerValues = actionContext.Request.Headers.FirstOrDefault(x => x.Key == ConfigurationManager.AppSettings["AuthKeyName"]).Value;
                if (headerValues.FirstOrDefault().ToLower() != ConfigurationManager.AppSettings["AuthKey"].ToLower())
                {
                    actionContext.Response = new System.Net.Http.HttpResponseMessage(System.Net.HttpStatusCode.Unauthorized);
                }
            }
        }
    }

}
