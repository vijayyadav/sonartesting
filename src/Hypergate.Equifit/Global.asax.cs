﻿using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace Hypergate.Equifit
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            GlobalConfiguration.Configure(WebApiConfig.Register);
            HttpConfiguration config = GlobalConfiguration.Configuration;
            config.Formatters.JsonFormatter.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
            config.Formatters.JsonFormatter.UseDataContractJsonSerializer = false;
        }

        protected void Application_Error()
        {
            Exception lastException = Server.GetLastError();
        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {
            var operationKey = Guid.NewGuid();
            var httpApplication = sender as System.Web.HttpApplication;
            System.Diagnostics.Trace.CorrelationManager.StartLogicalOperation(operationKey);
            var content = ProcessResponse(httpApplication.Request.InputStream);
            var context = new System.Web.HttpRequestWrapper(httpApplication.Request);
            // Set thread name to same as operationKey so that logger will record the id for each log entry
            if (string.IsNullOrWhiteSpace(System.Threading.Thread.CurrentThread.Name))
            {
                System.Threading.Thread.CurrentThread.Name = operationKey.ToString();
            }
        }

        protected void Application_EndRequest(object sender, EventArgs e)
        {
            HttpApplication application = (HttpApplication)sender;
            HttpResponse response = application.Context.Response;

        }

        private string ProcessResponse(Stream stream)
        {
            StreamReader sr = new StreamReader(stream);
            return sr.ReadToEnd();
        }
    }
}
