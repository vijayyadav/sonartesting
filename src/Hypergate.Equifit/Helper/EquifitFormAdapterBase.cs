﻿using Hypergate.Equifit.Models;

namespace Hypergate.Equifit.Helper
{
    public abstract class EquifitFormAdapterBase
    {        
        protected EquifitFormAdapterBase(EquifitForm form)
        {
            this.EquifitForm = form;
        }

        protected virtual EquifitForm EquifitForm { get; private set; }
    }
}