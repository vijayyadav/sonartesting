﻿using System;
using System.Linq;
using Hypergate.Equifit.Helper.Extensions;
using Hypergate.Equifit.Models;
using Newtonsoft.Json.Linq;

namespace Hypergate.Equifit.Helper
{
    public class EquifitFormAdapter: EquifitFormAdapterBase
    {
        private static readonly Func<EquifitQuestion, JObject> AppendSchemaProperties = question =>
        {
            var root = new JObject();
            var editorAttrs = new JObject();

            question
                .With(item => item.AnswerType)
                .With(item => item.InputType)
                .Do(item => root.Add(new JProperty("type", item)));
            question
                .With(item => item.Text)
                .Do(item => root.Add(new JProperty(item.ContainsHtml() ? "titleHTML" : "title", item)));
            question
                .With(item => item.AnswerType)
                .With(item => item.CssClass)
                .Do(item => root.Add(new JProperty("editorClass", item)));
            question
                .With(item => item.AnswerType)
                .With(item => item.Placeholder)
                .Do(item => editorAttrs.Add(new JProperty("placeholder", item)));
            question
                .With(item => item.AnswerType)
                .If(item => item.MaxLength.HasValue)
                .Do(type => editorAttrs.Add(new JProperty("maxlength", type.MaxLength.Value)));
            question
                .With(item => item.AnswerType)
                .With(item => item.ReadOnly)
                .Do(item => editorAttrs.Add(new JProperty("readonly", item)));
            question
                .With(item => item.AnswerType)
                .With(item => item.Disabled)
                .Do(item => editorAttrs.Add(new JProperty("disabled", item)));

            if (editorAttrs.HasValues)
            {
                root.Add(new JProperty("editorAttrs", editorAttrs));
            }

            question
                .With(item => item.CssClass)
                .Do(item => root.Add(new JProperty("fieldClass", item)));
            question
                .With(item => item.AnswerType)
                .With(item => item.Events)
                .If(item => item.Any())
                .Do(item => item.Each(@event => root.Add(new JProperty("fieldAttrs",
                    new JObject(
                        new JProperty("data-bind", @event.Action),
                        new JProperty("data-target",
                            @event.Targets.Any()
                                ? string.Join(",", @event.Targets
                                    .Select(target => target.Target)
                                    .ToArray())
                                : null),
                        new JProperty("data-condition",
                            @event.Conditions.Any()
                                ? string.Join(",", @event.Conditions
                                    .Select(condition => condition.Target)
                                    .ToArray())
                                : null))))));
            question
                .With(item => item.AnswerType)
                .With(item => item.AnswerOptions)
                .If(item => item.Any())
                .Do(options => root.Add(new JProperty("options",
                    new JArray(options.Select(option =>
                        new JObject(
                            new JProperty("val", option.Id),
                            new JProperty("label", option.DisplayText)))))));
            question
                .With(item => item.AnswerType)
                .With(item => item.Validators)
                .If(item => item.Any())
                .Do(validators => root.Add(new JProperty("validators",
                    new JArray(validators.Select(validator =>
                        new JObject(
                            new JProperty("type", validator.Type.ToName().ToLower()),
                            new JProperty("message", validator.ErrorMessage)))))));
            question
                .With(item => item.AnswerType)
                .If(item => item.HelpText.IsNotNullOrEmpty())
                .Do(type => root.Add(new JProperty("help", type.HelpText)));

            int? fixedStartYear = null;
            question
                .With(item => item.AnswerType)
                .If(item => item.StartYear.HasValue).Do(type => fixedStartYear = type.StartYear);

            //Make sure yearStart property included only once
            if (fixedStartYear.HasValue)
            {
                question.Do(type => root.Add(new JProperty("yearStart", fixedStartYear.Value)));
            }
            else
            {
                question
                    .With(item => item.AnswerType)
                    .If(item => item.StartYearOffset.HasValue)
                    .Do(type => root.Add(new JProperty("yearStart", DateTime.UtcNow.Year - type.StartYearOffset.Value)));
            }


            question
                .With(item => item.AnswerType)
                .If(item => item.EndYearOffset.HasValue)
                .Do(type => root.Add(new JProperty("yearEnd", DateTime.UtcNow.Year - type.EndYearOffset.Value)));

            return root;
        };

        private JArray _fieldsets;
        private JObject _schema;

        public EquifitFormAdapter(EquifitForm equifitForm)
            : base(equifitForm)
        {
            this.CreateSchemaFactory = CreateDefaultSchemaFactory();            
            this.CreateFieldSetsFactory = CreateDefaultFieldSetsFactory();            
        }

        public Func<EquifitForm, JObject> CreateSchemaFactory { get; set; }        
        public Func<EquifitForm, JArray> CreateFieldSetsFactory { get; set; }        

        public JObject Schema
        {
            get { return this._schema ?? (this._schema = this.CreateSchemaFactory(this.EquifitForm)); }
        }

        public JArray Fieldsets
        {
            get { return this._fieldsets ?? (this._fieldsets = this.CreateFieldSetsFactory(this.EquifitForm)); }
        }

        private static Func<EquifitForm, JObject> CreateDefaultSchemaFactory()
        {
            return form => new JObject(form.Sections
                .SelectMany(questions => questions.Questions
                    .Select(question =>
                        new JProperty(question.Id, AppendSchemaProperties(question)))));
        }        

        private static Func<EquifitForm, JArray> CreateDefaultFieldSetsFactory()
        {
            return form => new JArray(form.Sections.Select(section =>
                new JObject(
                    new JProperty("legend", section.Name),
                    new JProperty("fields", new JArray(section.Questions.Select(question => new JValue(question.Id)))))));
        }        
    }
}