﻿using System;
using System.Text.RegularExpressions;
using System.ComponentModel;

namespace Hypergate.Equifit.Helper.Extensions
{
    public static class StringExtensions
    {
        private static readonly Regex HtmlExpression = new Regex("<\\S[^><]*>",
            RegexOptions.IgnoreCase | RegexOptions.Singleline | RegexOptions.Multiline | RegexOptions.CultureInvariant |
            RegexOptions.Compiled);
        public static T SafeParse<T>(this string value)
        {
            T result = default(T);
            if (!string.IsNullOrEmpty(value))
            {
                try
                {
                    TypeConverter tc = TypeDescriptor.GetConverter(typeof(T));
                    result = (T)tc.ConvertFrom(value);
                }
                catch (Exception)
                {
                    result = default(T);
                }
            }
            return result;
        }

        public static DateTime? ToDateTime(this string s)
        {
            DateTime dateTime;
            bool isDateTime = DateTime.TryParse(s, out dateTime);
            return (isDateTime) ? dateTime : new DateTime?();
        }

        public static int TryParseInt(this string input, int defaultValue = 0)
        {
            int value;

            if (!string.IsNullOrEmpty(input) && Int32.TryParse(input, out value))
            {
                return value;
            }
            return defaultValue;
        }

        public static double TryParseDouble(this string input, double defaultValue = 0)
        {
            double value;

            if (!string.IsNullOrEmpty(input) && Double.TryParse(input, out value))
            {
                return value;
            }
            return defaultValue;
        }

        public static int? ToNullableInt(this string input)
        {
            int value;

            if (!string.IsNullOrEmpty(input) && Int32.TryParse(input, out value))
            {
                return value;
            }
            return null;
        }

        public static double? ToNullableDouble(this string input)
        {
            double value;

            if (!string.IsNullOrEmpty(input) && Double.TryParse(input, out value))
            {
                return value;
            }
            return null;
        }

        public static bool IsNullOrEmpty(this string instance)
        {
            return string.IsNullOrEmpty(instance);
        }


        public static bool IsNotNullOrEmpty(this string instance)
        {
            return !string.IsNullOrEmpty(instance);
        }

        public static bool IsNullOrWhiteSpace(this string instance)
        {
            return string.IsNullOrWhiteSpace(instance);
        }

        public static bool IsNotNullOrWhiteSpace(this string instance)
        {
            return !string.IsNullOrWhiteSpace(instance);
        }

        public static string IfEmpty(this string instance, string defaultValue)
        {
            return instance.IsNotEmpty() ? instance : defaultValue;
        }

        public static string NullIfEmpty(this string instance)
        {
            return instance.IsEmpty() ? null : instance;
        }

        public static string FormatWith(this string instance, params object[] values)
        {
            return string.Format(instance, values);
        }

        public static bool IsEmpty(this string instance)
        {
            return instance == string.Empty;
        }

        public static bool IsNotEmpty(this string instance)
        {
            return instance != string.Empty;
        }

        public static bool ContainsHtml(this string instance)
        {
            return HtmlExpression.IsMatch(instance);
        }

        public static T ToEnum<T>(this string instance, T defaultValue) where T : struct, IComparable, IFormattable
        {
            T convertedValue = defaultValue;

            if (instance.IsNotNullOrWhiteSpace() && !Enum.TryParse(instance.Trim(), true, out convertedValue))
            {
                convertedValue = defaultValue;
            }

            return convertedValue;
        }
    }
}