﻿using Hypergate.Equifit.Models;

namespace Hypergate.Equifit.Helper.Extensions
{
    public static class EquifitElementExtensions
    {
        public static string ToName(this EquifitElement element)
        {
            switch (element)
            {
                case EquifitElement.Document:
                    return "Document";
                case EquifitElement.Section:
                    return "Section";
                case EquifitElement.Description:
                    return "Description";
                case EquifitElement.Question:
                    return "Question";
                case EquifitElement.Content:
                    return "Content";
                case EquifitElement.Paragraph:
                    return "Paragraph";
                case EquifitElement.Answers:
                    return "Answers";
                case EquifitElement.Answer:
                    return "Answer";
                case EquifitElement.AnswerType:
                    return "AnswerType";
                case EquifitElement.AnswerOptions:
                    return "AnswerOptions";
                case EquifitElement.AnswerOption:
                    return "AnswerOption";
                case EquifitElement.Events:
                    return "Events";
                case EquifitElement.Event:
                    return "Event";
                case EquifitElement.Targets:
                    return "Targets";
                case EquifitElement.Target:
                    return "Target";
                case EquifitElement.Conditions:
                    return "Conditions";
                case EquifitElement.Condition:
                    return "Condition";
                case EquifitElement.Validators:
                    return "Validators";
                case EquifitElement.Validator:
                    return "Validator";
                case EquifitElement.Title:
                    return "Title";
                case EquifitElement.Text:
                    return "Text";
                default:
                    return string.Empty;
            }
        }
    }
}