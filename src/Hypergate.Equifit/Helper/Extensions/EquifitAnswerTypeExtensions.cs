﻿using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using Hypergate.Equifit.Models;

namespace Hypergate.Equifit.Helper.Extensions
{
    public static class EquifitAnswerTypeExtensions
    {
        public static IEnumerable<EquifitAnswerOption> ToAnswerOptions(this EquifitAnswerType instance, XElement element)
        {
            if (instance.IsNull() || element.IsNull())
                return null;

            return element.Descendants(EquifitElement.AnswerOption.ToName())
                .Select(xml => new EquifitAnswerOption(
                    xml.GetAttributeValueOrDefault<string>(EquifitAttribute.Id.ToName()),
                    xml.GetAttributeValueOrDefault<string>(EquifitAttribute.DisplayText.ToName())));
        }

        public static IEnumerable<EquifitAnswerEvent> ToAnswerEvents(this EquifitAnswerType instance, XElement element)
        {
            if (instance.IsNull() || element.IsNull())
                return null;

            return element.Descendants(EquifitElement.Event.ToName()).Select(xml => new EquifitAnswerEvent(xml));
        }

        public static IEnumerable<EquifitValidator> ToValidators(this EquifitAnswerType instance, XElement element)
        {
            if (instance.IsNull() || element.IsNull())
                return null;

            return element.Descendants(EquifitElement.Validator.ToName())
                .Select(
                    xml =>
                        new EquifitValidator(
                            xml.GetAttributeValueOrDefault<string>(EquifitAttribute.Type.ToName()).ToValidatorType(),
                            xml.GetAttributeValueOrDefault<string>(EquifitAttribute.ErrorMessage.ToName())));
        }
    }
}