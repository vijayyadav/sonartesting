﻿using Hypergate.Equifit.Models;

namespace Hypergate.Equifit.Helper.Extensions
{
    public static class EquifitValidatorTypeExtensions
    {
        public static string ToName(this EquifitValidatorType validator)
        {
            switch (validator)
            {
                case EquifitValidatorType.Required:
                    return "Required";
                default:
                    return string.Empty;
            }
        }

        public static EquifitValidatorType ToValidatorType(this string validator)
        {
            switch (validator)
            {
                case "Required":
                    return EquifitValidatorType.Required;
                default:
                    return EquifitValidatorType.Required;
            }
        }
    }
}