﻿using System;
using System.ComponentModel;

namespace Hypergate.Equifit.Helper.Extensions
{
    public static class ObjectExtensions
    {
        public static bool IsNull(this object instance)
        {
            return IsNull<object>(instance);
        }

        public static bool IsNull<TSource>(this TSource instance) where TSource : class
        {
            return instance == null;
        }

        public static bool IsNotNull(this object instance)
        {
            return IsNotNull<object>(instance);
        }

        public static bool IsNotNull<TSource>(this TSource instance) where TSource : class
        {
            return instance != null;
        }

        public static bool IsReferenceEqual(this object instance, object compareTo)
        {
            return IsReferenceEqual<object>(instance, compareTo);
        }

        public static bool IsReferenceEqual<TSource>(this TSource instance, object compareTo) where TSource : class
        {
            return ReferenceEquals(instance, compareTo);
        }

        public static TSource GetDefaultIfNull<TSource>(this TSource instance, TSource defaultValue)
            where TSource : class
        {
            return ReferenceEquals(instance, null) ? defaultValue : instance;
        }

        public static TSource GetDefaultIfNull<TSource>(this TSource instance, Func<TSource> defaultValueProvider)
            where TSource : class
        {
            return ReferenceEquals(instance, null) ? defaultValueProvider() : instance;
        }

        public static TResult With<TSource, TResult>(this TSource instance, Func<TSource, TResult> evaluator)
            where TResult : class
            where TSource : class
        {
            return ReferenceEquals(instance, null) ? null : evaluator(instance);
        }
        
        public static TSource If<TSource>(this TSource instance, Func<TSource, bool> evaluator) where TSource : class
        {
            if (ReferenceEquals(instance, null))
            {
                return null;
            }

            return evaluator(instance) ? instance : null;
        }
        
        public static TSource Unless<TSource>(this TSource instance, Func<TSource, bool> evaluator)
            where TSource : class
        {
            if (ReferenceEquals(instance, null))
            {
                return null;
            }

            return evaluator(instance) ? null : instance;
        }
        
        public static TSource Do<TSource>(this TSource instance, Action<TSource> action) where TSource : class
        {
            if (ReferenceEquals(instance, null))
            {
                return null;
            }

            action(instance);
            return instance;
        }
        
        public static TResult Return<TSource, TResult>(this TSource instance, Func<TSource, TResult> evaluator)
            where TSource : class
        {
            return !ReferenceEquals(instance, null) ? evaluator(instance) : default(TResult);
        }
        
        public static TResult Return<TSource, TResult>(this TSource instance, Func<TSource, TResult> evaluator,
            TResult defaultValue) where TSource : class
        {
            return !ReferenceEquals(instance, null) ? evaluator(instance) : defaultValue;
        }
        
        public static TResult Return<TSource, TResult>(this TSource instance, Func<TSource, TResult> evaluator,
            Func<TResult> defaultValueFactory) where TSource : class
        {
            return !ReferenceEquals(instance, null) ? evaluator(instance) : defaultValueFactory();
        }
        
        public static T ConvertTo<T>(this object instance)
        {
            return instance.ConvertTo(default(T));
        }
        
        public static T ConvertTo<T>(this object instance, T defaultValue)
        {
            if (instance != null)
            {
                Type targetType = typeof(T);

                if (instance.GetType() == targetType)
                {
                    return (T)instance;
                }

                TypeConverter converter = TypeDescriptor.GetConverter(instance);

                if (converter.IsNotNull())
                {
                    if (converter.CanConvertTo(targetType))
                    {
                        return (T)converter.ConvertTo(instance, targetType);
                    }
                }

                converter = TypeDescriptor.GetConverter(targetType);

                if (converter.IsNotNull())
                {
                    if (converter.CanConvertFrom(instance.GetType()))
                    {
                        return (T)converter.ConvertFrom(instance);
                    }
                }
            }

            return defaultValue;
        }
        
        public static bool CanConvertTo<T>(this object instance)
        {
            if (instance != null)
            {
                Type targetType = typeof(T);
                TypeConverter converter = TypeDescriptor.GetConverter(instance);

                if (converter.IsNotNull())
                {
                    if (converter.CanConvertTo(targetType))
                    {
                        return true;
                    }
                }

                converter = TypeDescriptor.GetConverter(targetType);

                if (converter.IsNotNull())
                {
                    if (converter.CanConvertFrom(instance.GetType()))
                    {
                        return true;
                    }
                }
            }

            return false;
        }
    }
}