﻿using Hypergate.Equifit.Models;

namespace Hypergate.Equifit.Helper.Extensions
{
    public static class EquifitAttributeExtensions
    {
        public static string ToName(this EquifitAttribute attribute)
        {
            switch (attribute)
            {
                case EquifitAttribute.Id:
                    return "Id";
                case EquifitAttribute.Name:
                    return "Name";
                case EquifitAttribute.Text:
                    return "Text";
                case EquifitAttribute.Version:
                    return "Version";
                case EquifitAttribute.DisplayOrder:
                    return "DisplayOrder";
                case EquifitAttribute.CssClass:
                    return "CssClass";
                case EquifitAttribute.Default:
                    return "Default";
                case EquifitAttribute.Placeholder:
                    return "Placeholder";
                case EquifitAttribute.ReadOnly:
                    return "ReadOnly";
                case EquifitAttribute.Disabled:
                    return "Disabled";
                case EquifitAttribute.IsPreEquifit:
                    return "IsPreEquifit";
                case EquifitAttribute.IsClientReport:
                    return "IsClientReport";
                case EquifitAttribute.Parent:
                    return "ParentId";
                case EquifitAttribute.InputType:
                    return "InputType";
                case EquifitAttribute.HelpText:
                    return "HelpText";
                case EquifitAttribute.DisplayText:
                    return "DisplayText";
                case EquifitAttribute.Action:
                    return "Action";
                case EquifitAttribute.Type:
                    return "Type";
                case EquifitAttribute.ErrorMessage:
                    return "ErrorMessage";
                case EquifitAttribute.StartYear:
                    return "StartYear";
                case EquifitAttribute.StartYearOffset:
                    return "StartYearOffset";
                case EquifitAttribute.EndYearOffset:
                    return "EndYearOffset";
                case EquifitAttribute.MaxLength:
                    return "MaxLength";
                default:
                    return string.Empty;
            }
        }
    }
}