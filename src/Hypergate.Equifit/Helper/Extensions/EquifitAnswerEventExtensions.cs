﻿using Hypergate.Equifit.Models;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;

namespace Hypergate.Equifit.Helper.Extensions
{
    public static class EquifitAnswerEventExtensions
    {
        public static IEnumerable<EquifitAnswerTarget> ToAnswerTargets(this EquifitAnswerEvent instance,
            XElement element)
        {
            if (instance.IsNull() || element.IsNull())
                return null;

            return
                element.Descendants(EquifitElement.Target.ToName())
                    .Select(xml => new EquifitAnswerTarget(xml.GetValueOrDefault<string>()));
        }

        public static IEnumerable<EquifitAnswerCondition> ToAnswerConditions(this EquifitAnswerEvent instance,
            XElement element)
        {
            if (instance.IsNull() || element.IsNull())
                return null;

            return
                element.Descendants(EquifitElement.Condition.ToName())
                    .Select(
                        xml =>
                            new EquifitAnswerCondition(
                                xml.GetAttributeValueOrDefault<string>(EquifitAttribute.Type.ToName())
                                    .ToEnum(EquifitConditionType.IfTrue), xml.GetValueOrDefault<string>()));
        }
    }
}