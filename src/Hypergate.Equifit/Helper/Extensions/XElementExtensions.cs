﻿using System.Xml.Linq;

namespace Hypergate.Equifit.Helper.Extensions
{
    public static class XElementExtensions
    {
        public static T GetAttributeValueOrDefault<T>(this XElement instance, string attributeName)
        {
            return GetAttributeValueOrDefault(instance, attributeName, default(T));
        }

        public static T GetAttributeValueOrDefault<T>(this XElement instance, string attributeName, T defaultValue)
        {
            return instance.IsNull() ? defaultValue : instance.Attribute(attributeName).GetValueOrDefault(defaultValue);
        }

        public static T GetValueOrDefault<T>(this XElement instance)
        {
            return GetValueOrDefault(instance, default(T));
        }

        public static T GetValueOrDefault<T>(this XElement instance, T defaultValue)
        {
            if (instance.IsNull())
            {
                return defaultValue;
            }

            T result = defaultValue;
            try
            {

                result = instance.Value.ConvertTo<T>();
            }
            catch
            {
            }
            return result;
        }
    }
}