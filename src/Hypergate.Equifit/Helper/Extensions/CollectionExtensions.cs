﻿using System;
using System.Collections.Generic;

namespace Hypergate.Equifit.Helper.Extensions
{
    public static class CollectionExtensions
    {
        public static bool IsNullOrEmpty<T>(this ICollection<T> instance)
        {
            return instance.IsNull() || instance.Count == 0;
        }

        public static bool IsNotNullOrEmpty<T>(this ICollection<T> instance)
        {
            return instance.IsNotNull() && instance.Count != 0;
        }

        public static bool IsEmpty<T>(this ICollection<T> instance)
        {
            if (instance == null)
            {
                return false;
            }
            return instance.Count == 0;
        }


        public static bool IsNotEmpty<T>(this ICollection<T> instance)
        {
            if (instance == null)
            {
                return false;
            }
            return instance.Count != 0;
        }

        public static void Each<T>(this IEnumerable<T> collection, Action<T> action)
        {
            if (collection == null || action == null)
            {
                return;
            }

            foreach (T item in collection)
            {
                action(item);
            }
        }
    }
}