﻿using System.Xml.Linq;

namespace Hypergate.Equifit.Helper.Extensions
{
    public static class XAttributeExtensions
    {
        public static T GetValueOrDefault<T>(this XAttribute instance)
        {
            return GetValueOrDefault(instance, default(T));
        }

        public static T GetValueOrDefault<T>(this XAttribute instance, T defaultValue)
        {
            if (instance.IsNull())
            {
                return defaultValue;
            }

            T result = defaultValue;
            try
            {
                result = instance.Value.ConvertTo<T>();
            }
            catch
            {
            }
            return result;
        }
    }
}