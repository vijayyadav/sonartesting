﻿using System.Net.Http;
using System.Web.Http;

namespace Hypergate.Equifit.Areas.V1.Controllers
{
    public partial class EquifitController
    {
        [HttpGet]
        [Route("v1/{country}/{memberId}/pre-eligibility")]
        public HttpResponseMessage CheckPreEquifitEligibility(string memberId, int country)
        {
            if (_repository == null)
                _repository = new Repository.Database.EquifitRepository();
            var response = _repository.GetPreEquifitEligibility(memberId, country);
            return Request.CreateResponse(System.Net.HttpStatusCode.OK, response);
        }
    }

}
