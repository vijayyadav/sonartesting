﻿using System.Net.Http;
using System.Web.Http;
using System.Collections.Generic;
using Hypergate.Equifit.Helper;
using Hypergate.Equifit.Models;
using System.Linq;

namespace Hypergate.Equifit.Areas.V1.Controllers
{
    public partial class EquifitController
    {
        [HttpGet]
        [Route("v1/pre-questionnaire")]
        public HttpResponseMessage GetPreEquifitQuestionnaire()
        {
            if (_repository == null)
                _repository = new Repository.Database.EquifitRepository();
            var preEquifitQuestionTemplates = _repository.GetPreEquifitQuestionnaire();
            var response = new PreEquifitQuestionnaireResponse();
            foreach (var template in preEquifitQuestionTemplates)
            {
                var equifitForm = new EquifitForm(template).Render();
                var adapter = new EquifitFormAdapter(equifitForm);
                var model = new EquifitDocumentFormsViewModel(adapter.Schema, adapter.Fieldsets, template.Id, template.Title, template.Version);
                equifitForm.Sections.ToList().ForEach(s =>
                {
                    model.QuestionsCount += s.Questions.Where(y => y.IsPreEquifit == true).Count();
                });
                response.TotalQuestions += model.QuestionsCount;
                response.Schemas.Add(model);
            }

            return Request.CreateResponse(System.Net.HttpStatusCode.OK, response);
        }
    }

}
