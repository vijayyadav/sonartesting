﻿using System.Web.Http;
using Hypergate.Equifit.Filters;
using Hypergate.Equifit.Repository.Database;

namespace Hypergate.Equifit.Areas.V1.Controllers
{
    [TokenAuth]
    public partial class EquifitController : ApiController
    {
        IEquifitRepository _repository;
        public EquifitController(IEquifitRepository repository)
        {
            _repository = repository;
        }

        public EquifitController()
        {

        }
    }

}
