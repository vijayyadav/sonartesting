﻿using System.Net.Http;
using System.Web.Http;
using Hypergate.Equifit.Areas.V1.Controllers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using Hypergate.Equifit.Models;

namespace Hypergate.Equifit.Tests.Integration
{
    [TestClass]
    public class EquifitTests
    {
        // Arrange
        string memberId = "1000066296";
        private int country = 1;

        // Arrange
        readonly EquifitController controller = new EquifitController
        {
            Request = new HttpRequestMessage(),
            Configuration = new HttpConfiguration()
        };

        [TestMethod]
        public void Get_PreEquifitEligibility_ShouldReturn_Eligibility()
        {
            // Act
            var response = controller.CheckPreEquifitEligibility(memberId, country);

            // Assert
            Assert.IsTrue(response.IsSuccessStatusCode);
            bool result;
            if (response.IsSuccessStatusCode)
            {
                var content = response.Content.ReadAsStringAsync();
                content.Wait();
                result = JsonConvert.DeserializeObject<bool>(content.Result);
                Assert.IsFalse(result);
            }
        }

        [TestMethod]
        public void Get_PreEquifitQuestionnaire_ShouldReturn_Questionnaire()
        {
            var response = controller.GetPreEquifitQuestionnaire();
            Assert.IsTrue(response.IsSuccessStatusCode);
            if(response.IsSuccessStatusCode)
            {
                var content = response.Content.ReadAsStringAsync();
                content.Wait();
                var result = JsonConvert.DeserializeObject<PreEquifitQuestionnaireResponse>(content.Result);
                Assert.IsNotNull(result);
            }

        }
    }
}
